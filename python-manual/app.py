# imports da aplicacao
import json
from time import sleep
from os import environ
from random import randint
from flask import Flask

from opentelemetry.sdk.resources import Resource

# metrics imports 
from opentelemetry import metrics
from opentelemetry.exporter.otlp.proto.http.metric_exporter import OTLPMetricExporter
from opentelemetry.sdk.metrics.export import (
    ConsoleMetricExporter,
    AggregationTemporality,
    PeriodicExportingMetricReader,
)
from opentelemetry.sdk.metrics import MeterProvider, Counter, UpDownCounter, Histogram, ObservableCounter, ObservableUpDownCounter

#traces imports 
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace import TracerProvider, sampling


# envs da aplicacao
DT_API_URL = environ['DT_API_URL']
DT_API_TOKEN = environ['DT_API_TOKEN']

#criacao de resource
service_name = dict()
data = open("/var/lib/dynatrace/enrichment/dt_host_metadata.json")
data = json.load(data)

service_name.update(data)

service_name.update({
    "service.name": "rolldice-man",
    "service.version": "2.0"
})

resource = Resource.create(service_name)

# exporter trace

tracer_provider = TracerProvider(sampler=sampling.ALWAYS_ON, resource=resource)
trace.set_tracer_provider(tracer_provider)

tracer_provider.add_span_processor(
    BatchSpanProcessor(
        OTLPSpanExporter(
            endpoint = DT_API_URL + "/v1/traces",
            headers = {
                "Authorization": "Api-Token " + DT_API_TOKEN
            }
        )
    )
)

# exporter metric
exporter = OTLPMetricExporter(
    endpoint = DT_API_URL + "/v1/metrics",
    headers = {"Authorization": "Api-token " + DT_API_TOKEN},
    preferred_temporality = {
        Counter: AggregationTemporality.DELTA,
        UpDownCounter: AggregationTemporality.CUMULATIVE,
        Histogram: AggregationTemporality.DELTA,
        ObservableCounter: AggregationTemporality.DELTA,
        ObservableUpDownCounter: AggregationTemporality.CUMULATIVE,
    }
)

# definicao spans

tracer = trace.get_tracer("diceroller.tracer")

# definicao metricas
metric_reader = PeriodicExportingMetricReader(exporter)
metric_provider = MeterProvider(metric_readers=[metric_reader], resource=resource)

metrics.set_meter_provider(metric_provider)

meter = metrics.get_meter("dicerollers.meter")

roll_counter = meter.create_counter(
    name="roll__counter_man",
    description="The number of rolls by roll value",
)

requests_time = meter.create_histogram(
    name="request_time_man",
    description="size of requests"
)

app = Flask(__name__)

@app.route("/rolldice")
def roll_dice():
    with tracer.start_as_current_span("/rolldice") as rollspan:
        res = do_roll()
        rqtime = make_request_time()

        return "dice: " + str(res) + " request time: " + str(rqtime)

def do_roll():
    with tracer.start_as_current_span("do_roll") as rollspan:
        res = randint(1,6)
        rollspan.set_attribute("roll.value", res)
        roll_counter.add(1, {"roll.value":res})

        return res

def make_request_time():
    with tracer. start_as_current_span("make_request_size") as rqtimespan:
        res = randint(0,300)
        rqtimespan.set_attribute("request_time", res)
        requests_time.record(res)
        sleep(res/100)

        return res
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
