# PYTHON MANUAL (EXECUÇÃO LOCAL)
### Projeto de exemplo para enviar métricas via OpenTelemetry para o Dynatrace


Este diretório possui implementação de um código escrito em Python considerando a instrumentação do open telemetry de forma **manual**. Logo, toda a configuração do open telemetry fica no código da aplicação.


## Passo a passo para configurar o ambiente

1. Exportar as variáveis de ambiente

- Copie o arquivo `envs.example`:

```
cp envs.example envs
```

- Edite o arquivo `envs` adicionando os valores

- Exporte as variáveis de ambiente:

```
source envs
```

2. Configurar ambiente virtual e instalar dependências:

```
make set
```


3. Executar o Flask:

```
make run-flask
```

## Comandos opcionais:

### Executar testes a cada 10 segundos:

```
make roll-dice
```

### Iniciar flask e executar testes a cada 10 segundos:

```
make run-test-env
```

### [Comando em desenvolvimento] Matar processo flask:

```
make kill-flask
``` 

### Ativar o ambiente virtual no shell em execução:

```
source activate
```