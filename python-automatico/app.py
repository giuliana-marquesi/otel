from opentelemetry import metrics

from random import randint
from flask import Flask

meter = metrics.get_meter("diceroller.meter")

roll_counter = meter.create_counter(
    name="roll_counter_auto",
    description="The number of rolls by roll value",
)


app = Flask(__name__)

@app.route("/rolldice")
def roll_dice():
    return str(do_roll())

def do_roll():
    res = randint(1,6)
    roll_counter.add(1, {"roll.value": res})
        
    return res
    

