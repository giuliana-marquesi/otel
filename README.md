[[_TOC_]]

# OTEL

Este projeto tem o foco em compartilhar o estudo em diferentes implementações do Open telemetry com Dynatrace.

Você pode utilizar para realizar um teste rápido em seu ambiente ou como base para seu próprio projeto.

**ATENÇÃO:** Como o foco inicial é nos estudos pessoais, o código poderá constantemente ser alterado e algumas vezes não estar rodando plenamente. Vou tentar manter o mais estável possível, mas não é uma garantia.

## CENÁRIOS

- [Python com OTEL manual (execução local)](./python-manual/)
- [Python com OTEL automático (execução local)](./python-manual/)
- [Python com OTEL automático (Kubernetes)](./python-k8s-manual/)
- [Springboot - OTEL automatico com Springboot Starter (Kubernetes)](./spring-k8s/automatico/springboot-starter)
- [Springboot - OTEL automatico com OTLP Export - Doc Dyna (Kubernetes)](./spring-k8s/automatico/dynatrace-otlp-export)
- [Springboot - OTEL automatico com OTLP Export - Doc Dyna - Excluindo namespace do monitoramento (Kubernetes)](./spring-k8s/automatico/dynatrace-otlp-export-wtnamespace)
- [Springboot - Sem OTEL](./spring-k8s/aplicacao-apenas)

### Planejado
---

Para o futuro pretendo incluir:

- [ ] Aplicação SpringBoot no Kubernetes com OTEL manual envio via API
- [X] Aplicação SpringBoot no Kubernetes com OTEL automático envio via API
- [ ] Aplicação SpringBoot no Kubernetes com OTEL manual envio via OneAagent
- [ ] Aplicação Springboot no Kubernetes com OTEL automático envio via OneAgent
- [ ] Aplicação Springboot no Kubernetes com OTEL automático envio via Collector
- [ ] Aplicação Springboot no Kubernetes com OTEL automático envio
- [ ] Implementar Collector no Kubernetes


## AMBIENTE

### PYTHON

O código em Python3 foi executando nas versões: `3.8.10`, `3.9.2`,  `3.11.5` e imagem `3.9-slim`


### SPRINGBOOT

O código utiliza java `17.0.9` e Springboot `v3.1.4`

### GERAL

Nos passo a passo dos README está sendo considerado o uso do programa `make` e `source`que são nativos do **Linux**. Ainda assim, executei a implementação manual no Windows e funcionou, nesse caso rodei os comandos que estão listados no arquivo Makefile (base para o comando `make`) e configurei as variáveis de ambiente manualmente que estão listadas no arquivo `local-env`. 

Internamente em alguns Makefiles o programa `envsubst` também é utilizado. Ele serve para substituir dinamicamente os valores existentes em variáveis de ambiente para os arquivos de configuração do deployment.
