# imports da aplicacao
from os import environ
from random import randint
from flask import Flask

from opentelemetry.sdk.resources import Resource

# metrics imports 
from opentelemetry import metrics
from opentelemetry.exporter.otlp.proto.http.metric_exporter import OTLPMetricExporter
from opentelemetry.sdk.metrics.export import (
    ConsoleMetricExporter,
    AggregationTemporality,
    PeriodicExportingMetricReader,
)
from opentelemetry.sdk.metrics import MeterProvider, Counter, UpDownCounter, Histogram, ObservableCounter, ObservableUpDownCounter

#traces imports 
from opentelemetry import trace
from opentelemetry.exporter.otlp.proto.http.trace_exporter import OTLPSpanExporter
from opentelemetry.sdk.trace.export import BatchSpanProcessor
from opentelemetry.sdk.trace import TracerProvider, sampling


# envs da aplicacao
DT_API_URL = environ['DT_API_URL']
DT_API_TOKEN = environ['DT_API_TOKEN']

#criacao de resource
service_name = dict()
service_name.update({
    "service.name": "rolldice-k8s",
    "service.version": "1.0"
})

resource = Resource.create(service_name)

# exporter trace

tracer_provider = TracerProvider(sampler=sampling.ALWAYS_ON, resource=resource)
trace.set_tracer_provider(tracer_provider)

tracer_provider.add_span_processor(
    BatchSpanProcessor(
        OTLPSpanExporter(
            endpoint = DT_API_URL + "/v1/traces",
            headers = {
                "Authorization": "Api-Token " + DT_API_TOKEN
            }
        )
    )
)

# exporter metric
exporter = OTLPMetricExporter(
    endpoint = DT_API_URL + "/v1/metrics",
    headers = {"Authorization": "Api-token " + DT_API_TOKEN},
    preferred_temporality = {
        Counter: AggregationTemporality.DELTA,
        UpDownCounter: AggregationTemporality.CUMULATIVE,
        Histogram: AggregationTemporality.DELTA,
        ObservableCounter: AggregationTemporality.DELTA,
        ObservableUpDownCounter: AggregationTemporality.CUMULATIVE,
    }
)

# definicao spans

tracer = trace.get_tracer("diceroller.tracer")

# definicao metricas
metric_reader = PeriodicExportingMetricReader(exporter)
metric_provider = MeterProvider(metric_readers=[metric_reader], resource=resource)

metrics.set_meter_provider(metric_provider)

meter = metrics.get_meter("dicerollers.meter")

roll_counter = meter.create_counter(
    name="roll_counter_man",
    description="The number of rolls by roll value",
)

app = Flask(__name__)

@app.route("/rolldice")
def roll_dice():
    return str(do_roll())

def do_roll():
    with tracer.start_as_current_span("do_roll") as rollspan:
        res = randint(1,6)
        rollspan.set_attribute("roll.value", res)
        roll_counter.add(1, {"roll.value":res})

        return res
    
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
