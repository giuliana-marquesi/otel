# PYTHON MANUAL (KUBERNETES)
### Projeto de exemplo para enviar métricas via OpenTelemetry para o Dynatrace em Kubernetes

Este diretório possui implementação de um código escrito em Python considerando a instrumentação do open telemetry de forma **manual**. Logo, toda a configuração do open telemetry fica no código da aplicação.

## Parar executar a primeira vez:

1. Configurar o arquivo envs

- Copiar arquivo envs.example:

```
cp envs.example envs

```

- Editar o arquivo `ènvs` criado no passo anteior com as informações necessárias:
	- DT_API_URL={ENDERECO ENVIRONMENT}/api/v2/otlp
	- DT_API_TOKEN={TOKEN}

2. Cria o deployment no kubernetes

```
make initial
```

Esse comando executa todos os comandos de configuração que estão a seguir: cria namespace, secrets e faz o deploy do serviço


## Comandos úteis: 


1. Criar a namespace

```
make namespace
```


2. Criar uma secret a partir do arquivo env

```
make secrets
```


3. Deploy do rolldice

```
make deploy
```


## Build image:


O deployment está configurado para utilizar a imagem e tag `giuliana/rolldice:latest`.

Caso você queira utilizar sua própria imagem ou necessite alterar o código e precise fazer um novo build, basta seguir esse passo a passo:


1. Criar uma imagem da aplicação

```
make build-image
```


2. Subir imagem no docker hub

- Logar no docker hub

```
docker login
```


- Taggear imagem com repositorio/user:

```
docker tag rolldice <seu-user-docker-hub>/rolldice
```


- Subir imagem no docker hub

```
docker push <seu-user-docker-hub>/rolldice
```
