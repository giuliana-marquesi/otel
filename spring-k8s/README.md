[[_TOC_]]

# SPRING (KUBERNETES)
### Projeto de exemplo para enviar trace (e talvez métricas) via OpenTelemetry para o Dynatrace em Kubernetes

Esta sessão com vários diretórios possui implementação de um código escrito em Spring. São diferentes implementações do Opentelemetry mantendo a aplicação igual. As mudanças no código são só relativas à inclusão do Opentelemetry.

## Tipos de implementação:

Todas as implementações estão considerando que os servidores estão com OneAgent instalado.

- [Apenas a aplicação, sem OTEL](./aplicacao-apenas/): Será monitorado pelo Dynatrace.
- [(Automatica) Implementação com Springboot Starter](./automatico/springboot-starter): Criará um serviço pelo OA. (Não sei se não funcionou a parte da configuração do OTEL).
- [(Automatica) Implementação com OTLP Export - Doc Dyna](./automatico/dynatrace-otlp-export): Será criado um serviço pelo OA e também pelo OTEL.
- [(Automatica) Implementação com OTLP Export - Doc Dyna - **Excluindo o namespce do monitoramento**](./automatico/dynatrace-otlp-export-wtnamespace): Será criado um serviço apenas pelo OTEL.


## Para executar a primeira vez:

A maioria das implementações vão seguir os mesmos comandos. Em geral, basta seguir esses passos. Em cada diretório terá o passo-a-passo novamente.

1. Configurar o arquivo envs (se tiver o arquivo envs.example no diretório)

- Copiar arquivo envs.example:

```
cp envs.example envs

```

- Editar o arquivo `envs` criado no passo anterior com as informações necessárias


2. Configurar o arquivo local-env 

- (**OPCIONAL**) Editar a variável de ambiente `DOCKER_HUB_USER` para seu usuário do Docker Hub, caso queira utilizar uma imagem personalizada. As outras variáveis manter como está configurado

- Configurar as variáveis de ambiente

```
source local-env
```

**IMPORTANTE: AS variáveis de ambiente mudarão de implementação por implementação. SEMPRE lembrar de executar o comando acima em cada projeto/implementação diferente**

3. Cria o deployment no kubernetes

```
make init-deployment
```

Esse comando executa todos os comandos de configuração que estão a seguir: cria namespace, cria secret (se precisar) e faz o deploy do serviço

---

## Comandos úteis: 


1. Criar a namespace

```
make namespace
```

2. Cria a secret com as variaveis de ambiente

```
make secret
```

3. Deploy da aplicação

```
make deploy
```

3. Rollout da aplicação

```
make rollout-deployment
```

4. Rodar container localmente

```
make run-container-local
```

---

## Build image:


Caso você queira utilizar sua própria imagem ou necessite alterar o código e precise fazer um novo build, basta fazer esse comando:

```
make publish-image
```

O comando acima executa as ações de `build-image`, `tag-image` e `push-image`.

**OBS**: Lembrando que você precisa estar logado na sua conta Docker Hub e alterar a variável de ambiente `DOCKER_HUB_USER` no arquivo local-env


### Comandos úteis para gerência da imagem docker:

1. Criar uma imagem da aplicação

```
make build-image
```


2. Logar no docker hub

```
docker login
```


3. Taggear imagem com repositorio/user:

```
make tag-image
```


4. Subir imagem no docker hub

```
make push-image
```
