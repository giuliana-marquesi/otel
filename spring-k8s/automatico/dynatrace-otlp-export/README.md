[[_TOC_]]

# SPRING  OTEL Automatico com OTLP Export (KUBERNETES)
### Projeto de exemplo para enviar trace (e talvez métricas) via OpenTelemetry para o Dynatrace em Kubernetes

Este projeto/diretório tem uma aplicação REST básica de ["Hello, World"](https://spring.io/guides/gs/rest-service/).

## Aplicação:

A aplicação apenas retona um json com os campos `id` e `content`. O `id` é um contador que itera a cada requisição. O `content` Retorna a frase "Hello, world" ou "Hello, `name`".

O único endpoint é o `greeting`.

Para testar sua aplicação. Acesse o endereço onde foi publicado com a porta de acordo com a definida pelo arquivo `local-env`.

```
[dominio]:8082/greeting
```

ex: `http://192.168.1.2:8080/greeting`

A aplicação também recebe o atributo `name` como query string.

ex: `http://192.168.1.2:8080/greeting?name=Giuliana`

## Implementação do OTEL

Este cenário contempla uma instrumentação automática do OTEL e enviando os dados para o Dynatrace via API pelo protocolo OTLP. Foram seguidas as orientações [desta página de documentação](https://docs.dynatrace.com/docs/extend-dynatrace/opentelemetry/walkthroughs/java/java-auto#tabgroup--dynatrace-docs--otlp-export), porém, alterada a variável de ambiente `OTEL_RESOURCE_ATTRIBUTES` para `OTEL_SERVICE_NAME`.

Neste cenário, como existe a monitoração do OneAgent nativa e a instrumentação via OTEL, serão criados dois serviços no Dynatrace, o `GreetingController` e o `dtc-otlp-export`


## Para executar a primeira vez:


1. Configurar o arquivo local-env 

- (**OPCIONAL**) Editar a variável de ambiente `DOCKER_HUB_USER` para seu usuário do Docker Hub, caso queira utilizar uma imagem personalizada. As outras variáveis manter como está configurado

- Configurar as variáveis de ambiente

```
source local-env
```

**IMPORTANTE: AS variáveis de ambiente mudarão de implementação por implementação. SEMPRE lembrar de executar o comando acima em cada projeto/implementação diferente**

2. Cria o deployment no kubernetes

```
make init-deployment
```

Esse comando executa todos os comandos de configuração que estão a seguir: cria namespace, cria secret e faz o deploy do serviço

---

## Comandos úteis: 


1. Criar a namespace

```
make namespace
```


2. Criar secret para configurar as variaveis de ambiente

```
make secret
```

3. Deploy da aplicação

```
make deploy
```

3. Rollout da aplicação

```
make rollout-deployment
```

4. Rodar container localmente

```
make run-container-local
```

---

## Build image:


Caso você queira utilizar sua própria imagem ou necessite alterar o código e precise fazer um novo build, basta fazer esse comando:

```
make publish-image
```

O comando acima executa as ações de `build-image`, `tag-image` e `push-image`.

**OBS**: Lembrando que você precisa estar logado na sua conta Docker Hub e alterar a variável de ambiente `DOCKER_HUB_USER` no arquivo local-env


### Comandos úteis para gerência da imagem docker:

1. Criar uma imagem da aplicação

```
make build-image
```


2. Logar no docker hub

```
docker login
```


3. Taggear imagem com repositorio/user:

```
make tag-image
```


4. Subir imagem no docker hub

```
make push-image
```
