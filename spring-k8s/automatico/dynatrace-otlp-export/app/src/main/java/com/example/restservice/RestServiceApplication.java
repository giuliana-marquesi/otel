package com.example.restservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.context.Scope;

@SpringBootApplication
public class RestServiceApplication {

	public static void main(String[] args) {

		Tracer tracer = GlobalOpenTelemetry
   			.getTracerProvider()
			.tracerBuilder("main-tracer") //TODO Replace with the name of your tracer
			.build();

		Span span = tracer.spanBuilder("main").startSpan();
		span.setAttribute("atributo", "main");
		
		try (Scope scope = span.makeCurrent()) {
			SpringApplication.run(RestServiceApplication.class, args);
		} finally {
			span.end();
		}
	}

}
