package com.example.restservice;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.opentelemetry.api.GlobalOpenTelemetry;
import io.opentelemetry.api.trace.Span;
import io.opentelemetry.api.trace.Tracer;
import io.opentelemetry.context.Scope;

@RestController
public class GreetingController {

	Tracer tracer = GlobalOpenTelemetry
	    .getTracerProvider()
	    .tracerBuilder("my-tracer")
	    .build();


	private static final String template = "Hello, %s!";
	private final AtomicLong counter = new AtomicLong();

	@GetMapping("/greeting")
	public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {

		Span span = tracer.spanBuilder("Call to /greeting").startSpan();

		span.setAttribute("atributo", "teste");
		
		try (Scope scope = span.makeCurrent()) {
			return new Greeting(counter.incrementAndGet(), String.format(template, name));
		}
		finally {
			span.end();
		}
	}
}
